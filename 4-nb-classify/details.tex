\renewcommand{\blockName}{Classification using Naive Bayes}

\renewcommand{\blockSynopsis}{
	In this chapter, we present an overview of the use of Bayes' Theorem in item classification, and explore some example models for document categorisation.
	We assume a basic understanding of Bayes' Theorem and probability (including the multinomial distribution) throughout the chapter.
}

\renewcommand{\blockContent}{
	\section{Going Probabalistic}

	The \(k\)-nearest neighbour method covered in the previous chapter has several flaws: it is inefficient in terms of computational complexity (\(O(1)\) amortised worst case time (in Python) to train; but \(O(mn)\) to classify \(n\) feature vectors against \(m\) training vectors), and further, it doesn't model uncertainty.

	When we say uncertainty, we mean the fact we may have inaccurate or incomplete information (our features vectors may cover the situation incompletely or may be comprised of measurements with inaccuracy or uncertainty themselves, especially in the case of physical measurements), and that we may have ignored other sources of information (for example, if we were using a learning system on some form of autonomous or semi-autonomous robot, it might be the case that we could model the world more accurately if we added a new kind of sensor, but we haven't and must account for this).

	To attempt to account for uncertainty, we can use the mathematics of probability to allow us to describe how the items described by our feature vectors tend to be distributed over our feature space (for example, if we have neck lengths of mammals as a component of our feature vectors, mammals with long necks are probably quite likely to be giraffes). This allows us to encode our assumptions about the items in a much more compact way, we need only store the parameters that define the distributions for each of our classes, instead of every single element of training data. Conveniently, this also deals with another problem of \(k\)-NN: using probablities helps to alleviate sensitivity to local structure of the data (for example, if we have a ridiculous giraffe that is very long necked but otherwise similar to a zebra, rather than it's fellow giraffes, \(k\)-NN may tend to classify it as a zebra, as it could be closer to the zebra data points than the giraffe ones; however, we may be able to tune our distributions such that our assumption about giraffes and their necks means that the long neck of our silly animal causes it to be recognised as a giraffe).

	\section{Probability Estimation}

	So now that we've decided to take a probabalistic approach to classification, we need some way of calculating our probabilities. If we've got a set of training data where each feature vector in the set is labelled with it's intended output class, calculating the probability \(P(\mathbf{x}|c)\) for some feature vector \(\mathbf{x}\) being a member of some class \(c\) (more strictly, it's the probability that a randomly chosen vector would be \(\mathbf{x}\) given we know the class of the vector we're picking is \(c\)) is simply the {\em relative frequency}, or in other words, the number of times the vector occurs in the training set with class \(c\) over the number of vectors in the training set with class \(c\). This is known as the {\bf likelihood} of class \(c\) given data \(\mathbf{x}\).

	However, we have a problem. Imagine an example 2 class scenario in which we are 10 times more likely to encounter items of the first class than the second. How do we account for this in our model? We need to account for the probablitity of seeing an object of a given class, the {\bf prior}, \(P(\mathbf{c})\).

	\section{Applying Bayes' Theorem}

	Now we have our likelihood and prior, how do we classify our points. Intuitively, we'd like to find the class which maximises the probability \(P(c|\mathbf{x}\), the probability that if we have the vector \(\mathbf{x}\) then the class is \(c\). This is also known as the {\bf posterior} probability.

	Now, by Bayes' Theorem, we have:

	\[P(c|\mathbf{x}) = \frac{P(\mathbf{x}|c)P(c)}{P(\mathbf{x})}\]

	Or in a more memorable, wordy form:

	\[Posterior \propto Likelihood \times Prior\]

	The term on the denominator is given simply by the law of total probability: divide the number of occurrences of of \(\mathbf{x}\) by the total number of vectors in the training set.

	This entire procedure is known as {\bf maximum a posteriori (MAP) decision rule}.

	\section{The Curse of Dimensionality}

	The MAP decision rule is all well and good, but it has some problems. One of the biggest of these is that as the number of dimensions of our feature vectors grow, the amount of potential values rises exponentially, and we require many, many more examples to obtain anything resembling a representative sample of the space. This problem is called the {\bf curse of dimensionality}, a phrase coined by Richard Bellman in the 1950s, and it poses a severe issue to applying machine learning to tasks which require feature vectors larger than a few dimensions.

	How do we solve this problem then? One way is to make a very silly assumption: assume each dimension is independent. Instead of constructing relative frequencies for each possible vector directly, we instead can construct the relative frequencies for each component of our \(n\)-dimensional vector and get the product, as shown below:

	\[P(\mathbf{x}|c) = P(x_1, x_2, \dots, x_n| c) \]

	By repeated application of the product rule, we get:

	\[P(\mathbf{x}|c) = P(x_1|x_2, \dots, x_d, c)P(x_2|x_3, \dots, x_d, c)\dots P(x_n|c) \]

	And since we are assuming each dimension is independent, we finally reduce to:

	\[P(\mathbf{x}|c) \approx P(x_1|c)P(x_2|c)\dots P(x_n|c) \]

	This is called the {\bf Naive Bayes} (or {\em Idiot Bayes}) assumption, and it is extremely drastic and rarely true in practice. For example with our giraffe example from earlier, it would be sensible to assume that disproportionate giraffes are rare and that there tends to be a relation between length of neck and height of giraffe. Those variables are clearly not independent then, but under the Naive Bayes assumption we're effectively saying that they are.

	Similar to earlier, we can represent assumption as a proportionality:

	\[P(\mathbf{x}|c) \approx \frac{P(c)\prod_{i=1}^{N}P(x_i|c}{\prod_{i=1}^{N}P(x_i)})\]

	\[P(\mathbf{x}|c) \propto P(c)\prod_{i=1}^{N}P(x_i|c)\]

	\section{Document Classification}

	As a more practical example to round out the chapter, we will examine the application of {\bf document classification}, also known as {\bf text classification}. This is the task of organising documents into categories (classes) based only on the words that they contain. This idea is applicable to many things, one notable example being {\em spam filtering}.

	If we have a vocabulary set \(V\), containing \(|V|\) word types, then our documents will equally be represented by vectors of length \(|V|\).

	We'll be using the Naive Bayes assumption to simplify the maths behind the classification, which means we have to find the class \(c\) with maximum posterior probability for document \(\mathbf{d}\) according to the following relation:

	\[P(\mathbf{d}|c) \propto P(c)\prod_{i=1}^{N}P(d_i|c)\]

	Within the scope of these notes, there are two formats for representation of documents we will cover:
		\begin{itemize}
			\item {\bf Bernoulli document model}: A document is represented by a feature vector of Bernoulli trials, where the \(i\)th element of the vector is 1 if the \(i\)th word in the vocabulary appears in the document, and is 0 otherwise.
			\item {\bf Multinomial document model}: A document is represented by a feature vector in which the \(i\)th element of the vector is a frequency count of the \(i\)th word in the vocabulary.
		\end{itemize}

	\section{Bernoulli Document Model}

	In the Bernoulli document model, as explained above, our documents are represented by \(|V|\)-dimensional vectors in which the \(i\)th element of each vector is 1 if and only if the document contains at least one occurence of the \(i\)th word in the vocabulary. If it does not, the element is zero.

	Let the \(i\)th document \(D_i\) be reprsented by a feature vector \(\mathbf{d}_i\) with the above property (so, \(d_{it}\) is 1 if word \(t\) in the vocabulary is present), and let \(P(w_t|C\) be the probability of the word \(w_t\) occuring in a document of class C. As we are modelling the presence of words as a Bernoulli trial (that is, the word is either there or not), the probability the word does not occur in a document of this class is simply \((1 - P(w_t|C))\). Using the Naive Bayes assumption, we can write the probability the document \(D_i\) belongs to class C (it's likelihood) as:

	\[P(D_i|C) = P(\mathbf{d}_i|C) \approx \prod_{t=1}^{|V|}[d_{it}P(w_t|C) + (1 - d_{it})(1 - P(w_t|C))]\]

	In words, this means that for each word \(t\) in our vocabulary \(V\), if the word is in the document, we take the probability of the word appearing in a document of class \(C\), otherwise we take the probability of it not appearing in such a document. We then take the product of the probabilities we have selected, and this product approximates the probability that our document \(D_i\) is in class \(C\).

	The probability that a word is in a document of class \(C\) is simply given by the number of documents of that class the word appears in (\(n_C(w_t)\)) over the number of documents of that class (\(N_C\)):

	\[P(w_t|C) = \frac{n_C(w_t)}{N_C}\]

	The prior for a class is also fairly easy to compute, being:

	\[P(C) = \frac{N_C}{N}\]

	Given all this information, we can use the MAP decision rule as before, selecting the the class with the highest posterior probability.

	\section{Multinomial Document Model}

	With the Multinomial document model, as explained two sections prior, our feature vectors represent the frequency of words within a document (not just their presence or lack thereof). The \(i\)th element is the frequency of the \(i\)th word of the vocabulary.

	Under the multinomial model, we have our document \(D_i\) as before, this time containing \(n_i\) words (the sum of the elements of our vector). By the multinomial distribution (since we're dealing with the chance of finding a sequence of words with the counts given in the vector), we have our likelihood given by:

	\[P(D_i|C) = P(\mathbf{d}_i|C) = \frac{n_i!}{\prod_{t=1}^{|V|}d_{it}!}\prod_{t=1}^{|V|}P(w_t|C)^{x_{it}}\]

	We can then compute the probability \(P(w_t|C)\) as the number of times the word \(t\) is used in any document of class \(C\) over the number of words in documents of class \(C\). We can represent this mathematically by defining an {\em indicator variable} \(z_{iC}\) which is 1 when document \(i\) is in class \(C\) and 0 otherwise, and then using it in the following formula:

	\[P(w_t|C) = \frac{\sum_{i=1}^{N}d_{it}z_{iC}}{\sum_{s=1}^{|V|}\sum_{i=1}^{N}x_{is}z_{iC}}\]

	Once again, we can use the MAP decision rule as before, noting that our prior probability is defined as it was before for the Binomial document model.

	\section{Problems with Relative Frequency}

	Using relative frequency estimates is a very simple, easy to reason about model of probability, but it suffers from two big drawbacks: the first that we have already covered is that a large amount of data is required for our training sample to representative of the data space on the whole; and the second is that items that never appear in our training data are thought never to occur by the system.

	Of course, in almost all cases, there will exist items that do not exist in our training data but are not impossible outcomes, perhaps very rare outcomes or perhaps our sample is simply not large enough. In essence, the system {\em underestimates} our probabilities. To attempt to make our estimate more accurate in the face of these unseen-yet-possible outcomes, we need to perform a process known as {\bf smoothing}.

	One simple method for smoothing is called {\em Laplace's law of succession} or {\bf add one smoothing}. Under this method, we simply pretend we've seen the entire set of outcomes once already, in effect adding \(n\) observations to our total and 1 observation to each of the outcomes. To relate it to our document classification problem, we have seen the entire vocabulary already (add \(|V|\) to our total count) and we thus have seen each word atleast once (add 1 to each word count).

	If \(m\) is the total number of observed outcomes, \(N\) is the total number of possible outcomes and \(C(x)\) is the total number of times \(x\) has been observed, our prior probability (based on relative frequency) would normally be given by:

	\[P(x) = \frac{C(x)}{m}\]

	But under add one smoothing becomes:

	\[P(x) = \frac{1 + C(x)}{N + m}\]

	This can be extended to posterior probabilities too, all we would need to do is change our definition to use \(N_c\), the total number of observed outcomes of class \(c\) and \(C(x,c)\), the total number of times \(x\) has been observed in class \(c\):

	\[P(x|c) = \frac{1 + C(x,c)}{m + N_c}\]

	\begin{framed}
	{\bf Better Smoothing}: Another method is called {\bf absolute discounting}, under which we subtract some small probability \(k\) from each nonzero probability, and redistribute it over all the unseen (zero probability) events.

	So if we let \(C(x,c)\) equal the total number of times \(x\) has been observed in class \(c\) once again, let \(N_c\) represent the total number of observed outcomes of class \(c\) and let \(u(n)\) be the number of outcomes which have been observed \(n\) times, we can define a modified count function, \(C'(x,c)\) like so:

	\[C'(x,c) = C(x,c) - k \quad \textrm{if C(x,c) > 0}\]
	\[C'(x,c) = \frac{k\sum_{i > 0}u(i)}{u(0)} \quad \textrm{if C(x,c) = 0}\]

	This means the posterior probability can be written like so:

	\[P(x|c) = \frac{C'(x,c)}{N_c}\]

	In practice, absolute discounting is significantly better than add one smoothing. As the size of our unseen events grows, large amounts of probability mass end up being used to represent the unseen events, and the probabilities of the events we have seen become increasingly small. In other words, add one smoothing has the effect of making the distribution for all events more uniform as the number of unseen events grows, meaning unseen events become almost as likely as events we have seen. 

	Absolute discounting avoids this, as only a fixed probability mass is used and redistributed amongst the unseen events, the relative likelihoods of the observed events stays fairly consistent against the likelihoods of the unseen events.
	\end{framed}

}