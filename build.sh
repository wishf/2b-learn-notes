#!/bin/sh

if [ $# -lt "1" ];
then
	echo "2B Learning Notes Build Script"
	echo "Requires a LaTeX distribution (see dependencies in README)"
	echo "Usage: ./build.sh [-v] [book|section <section name>]"
	echo "Valid section names are:"
    ls -1d */ | sort -n | sed '/auxf/d; /build/d; s/\/$//g; s/^[0-9]*-//g; s/^/- /g'
    echo "- all"
	exit 1
fi

if [ "$1" == "-v" ];
then
	if [ $# -lt "2" ];
	then
		echo "2B Learning Notes Build Script"
		echo "Requires a LaTeX distribution (see dependencies in README)"
		echo "Usage: ./build.sh [-v] [book|section <section name>]"
		echo "Valid section names are:"
		ls -1d */ | sort -n | sed '/auxf/d; /build/d; s/\/$//g; s/^[0-9]*-//g; s/^/- /g'
		echo "- all"
		exit 1
	fi

	quiet=""
	v="-v"
	cmd="$2"
	if [ $# -lt "3" ];
	then
		sect=""
	else
		if [ "$3" == "all" ];
		then
			sect="all"
		else
			sect="$(ls -1d */ | sort -n | grep $3)"
		fi
	fi
else
	quiet="-quiet"
	v=""
	cmd="$1"
	if [ $# -lt "2" ];
	then
		sect=""
	else
		if [ "$2" == "all" ];
		then
			sect="all"
		else
			sect="$(ls -1d */ | sort -n | grep $2)"
		fi
	fi
fi

data="$(ls -1d */ | sort -n | sed '/auxf/d; /build/d; s/\/$//g')"

if [ $cmd == "book" ];
then
	cp book-template.tex book.tex
	for s in $data
	do
		cat book.tex | sed 's/% --- %/\\input{.\/'"$s"'\/details.tex}\n\\input{.\/chapter-template.tex}\n% --- %/g' > book-t.tex
		cp book-t.tex book.tex
		rm -f book-t.tex
	done
	pdflatex --output-directory=build -aux-directory=auxf $quiet book.tex
	# for table of contents
	pdflatex --output-directory=build -aux-directory=auxf $quiet book.tex
	rm -f book.tex
fi

if [ $cmd == "section" ];
then
	if [ "$sect" == "all" ]
	then
		for s in $data
		do
			./build.sh $v section "$s"
		done
		exit 1
	fi

	if [ -e $sect ]
	then
		if [ ! -e "build/" ];
		then
			mkdir build
		fi

		if [ ! -e "auxf/" ];
		then
			mkdir auxf
		fi

		sectClean="$(echo $sect | sed 's/\/$//g; s/^[0-9]*-//g')"

		echo $sect

		pdflatex --job-name=$sectClean --output-directory=build --aux-directory=auxf $quiet "\newcommand{\sectionFile}{./"$sect"details.tex}\input{section-standalone.tex}"
	else
		echo "2B Learning Notes Build Script"
		echo "Requires a LaTeX distribution (see dependencies in README)"
		echo "Usage: ./build.sh [-v] [book|section <section name>]"
		echo "Valid section names are:"
		ls -1d */ | sort -n | sed '/auxf/d; /build/d; s/\/$//g; s/^[0-9]*-//g; s/^/- /g'
		echo "- all"
		exit 1
	fi
fi
