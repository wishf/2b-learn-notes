2b-learn-notes
----------

My 2B learning thread notes - cleaned up and LaTeX-ified as part of my revision.

Each section gets it's own folder, structured as follows:

* _similarity_ - Similarity and Recommender Systems
* _clustering_ - Clustering and collaborative filtering
* _nn-classify_ - Classification (using k-nearest neighbour)
* _nb-classify_ - Classification (using Naive Bayes)
* _gaussians_ - Primer on Gaussian distributions
* _gauss-classify_ - Using Gaussian distributions for classification
* _discriminant_ - Discriminant functions
* _neural_ - Neural networks

The idea is that it might be nice to stitch these together into a mini-book of sorts.

Probability is used in these notes, so it might be worthwhile reading up on it.
I do have my probability notes in a similar format, see prob-notes.

Dependencies
------------

Currently, this document depends on the following LaTeX packages:

* mathtools
* multirow
* booktabs
* framed

Building
--------

Use the provided ```build.sh``` file to construct PDFs of either individual sections or the entire mini-book.

Usage information is below, but can also be retrieved by calling build.sh with no arguments:

```
./build.sh [-v] [book|section <section name>]
```