\renewcommand{\blockName}{Intro to Gaussians}

\renewcommand{\blockSynopsis}{
	This chapter provides a short overview on univariate and multivariate Gaussian distributions. However, it does assume some basic knowledge of probability.
}

\renewcommand{\blockContent}{
	\section{Gaussians in a Single Dimension}

	The {\bf Gaussian} (or {\em Normal}) distribution is one of the most frequently used continuous probability distributions (in part because it models many real world situations quite well, and also because many discrete distributions tend towards a normal distribution under certain conditions).

	A scalar random variable which takes on values under a Gaussian distribution has the probability of it taking on a given value described by:

	\[p(x|\mu, \sigma^2) = N(x;\mu,\sigma^2) = \frac{1}{\sqrt{2\pi\sigma^2}}e^\frac{-(x-\mu)^2}{2\sigma^2}\]

	\begin{framed}
	{\bf \em Note on notation}: In the above, it is worth noting that the course uses \(p(x|\mu, \sigma^2)\) to mean {\em the probability that the value x occurs given a Gaussian distribution with mean \(\mu\) and variance \(\sigma^2\)}. It is {\bf NOT} a joint distribution. 
	\end{framed}

	All Gaussians produce the same distinctive bell curve shape. It can be useful to view the mean \(\mu\) as the location of the Gaussian's peak, and the variance \(\sigma^2\) as the spread or dispersion; how "fat" the tails are on the Gaussian (and also how high the peak is: peak height is inversely proportional to variance). Gaussians with higher variance will have their tails approach zero much more slowly than distributions with lower variance.

	\begin{framed}
	{\bf \em Formula Dissection}: In the formula for a Gaussian distribution, it can be useful to view the \(\frac{1}{\sqrt{2\pi\sigma^2}}\) term as a way to {\em normalise} the distribution and ensure it integrates to 1 (thus making it a true probability distribution). The \(\frac{-(x-\mu)^2}{2\sigma^2}\) term used as the exponent can be seen as dictating the location of the peak (the \(-(x-\mu)^2\) term will be zero when \(x = \mu\), so the \(e\) term as a whole will evaluate to 1, the highest value it can take). As \(x\) drifts from \(\mu\) on either side, the difference increases, and so the \(e\) term moves closer to zero. How rapidly this happens is dictated by the magnitude of \(\sigma^2\), which can be seen to "compress" the difference above in the exponent term.
	\end{framed}

	\section{Parameter Estimation for Univariate Gaussians}

	When using Gaussians in a machine learning context, we are not given the values of the parameters \(\mu\) and \(\sigma^2\), we are only given raw training data. So in order to model this data using a Gaussian distribution, we must first {\em estimate} the value of these parameters. To do this we use the {\em maximum likelihood estimate} of the mean and variance, which are the {\bf sample mean} and {\bf sample variance} respectively:

	\[\mu = \frac{1}{n}\sum_{i=1}^{n} x_i\]

	\[\sigma^2 = \frac{1}{n}\sum_{i=1}^{n} (x_i - \mu)^2\]

	These formulas assume a training set of size \(n\), where \(x_i\) is the (scalar) feature value of the \(i\)th term.

	\begin{framed}
	{\bf \em Note on Sample Variance}: Normally, we would use a divisor of \(n-1\) in the formula for the sample variance, as using the sample mean in the formula with \(n\) as our divisor causes the sample variance to become a {\em biased estimator}, underestimating the variance by a factor of \(\frac{n-1}{n}\). Using \(n-1\) as a divisor eliminates this bias (this is known as {\bf Bessel's correction}). However, as we are looking for a maximum likelihood estimate, it turns out we would much rather use the biased form, wtih \(n\) as our divisor.
	\end{framed}

	\section{Gaussians in Multiple Dimensions}

	In the sections above, we looked at the Gaussian distribution in a single dimension (the {\em univariate} case) but it is possible, and indeed very useful, to extend our definition of the Gaussian distribution to multiple dimensions (the {\bf multivariate} case).

	To do so, we must first decide on how to represent our mean and variance in multiple dimensions. For the mean, this is quite simple, we simply define \(\mathbf{\mu}\) to be a {\bf mean vector}, a vector in which each component is the mean value for that dimension (or in other words, since our training data is feature vectors, the \(i\)th component of \(\mathbf{\mu}\) is simply the mean of the \(i\)th components of the vectors in our training data). It is useful to know that since the mean vector \(\mathbf{\mu}\) is the expectation of our training data, the method we have just covered is effectively an estimate of expectation.

	Extending variance to multiple dimensions however is somewhat more tricky. In the univariate case, we were only examining how our variable varied against itself, but in multiple dimensions, we must be aware of how each variable varies with every other variable (including itself). To do this, we will need to construct a square matrix (\(n\) by \(n\), where \(n\) is the number of dimensions of our feature vectors) where each row and column would be labelled by the name of a variable, and the entry in row \(i\) and column \(j\) is the {\bf covariance} between the variables \(i\) and \(j\). Hence why this matrix is known as the {\bf covariance matrix}. In the case where \(i\) and \(j\) are the same variable (along the leading diagonal; from top left to bottom right), the entry is simply the variance for that variable (as it is in the univariate case). We assign the covariance matrix to \(\Sigma\), be careful not to confuse it for a summation!

	We use the notation \(\sigma_{ij}\) to refer to the covariance of the variables with row \(i\) (say, \(X\)) and column \(j\) (say, \(Y\)) in the matrix. Sometimes, covariance is also written in terms of the random variables directly, as \(Cov(X,Y)\).

	\begin{framed}
	{\bf \em Interpreting Covariance}: By examining the sign and magnitude of a given covariance between two variables (say, X and Y), we can get a grasp on how these variables relate. A negative value of covariance implies the variables are {\em inversely} related; while a positive value implies they are {\em directly} related. The magnitude gives us a way of expressing how strongly the variables are related, with 0 implying absolutely no relation.
	\end{framed}

	Since the covariance between two variables \(i\) and \(j\) will be the same no matter which labels the row and which labels the column, we can also see that our covariance matrix must be symmetric (that is, the entry at (\(i\),\(j\)) will be the same as the entry in (\(j\), \(i\))).

	\begin{framed}
	{\bf \em Extra Info}: Given the above information, it can be seen that the univariate case is simply a special case of the multivariate, with a 1x1 covariance matrix. As the matrix is only 1 by 1, the {\em only} entry must be the variance for the variable, as there are no other variables to vary with!
	\end{framed}

	So, now that we have our representation of mean and covariance settled, we can now see how this fits together in the form of an equation describing the distribution:

	\[p(\mathbf{x}|\mathbf{\mu},\mathbf{\Sigma}) = \frac{1}{(2\pi)^{d/2}|\mathbf{\Sigma}|^{1/2}}e^{-\frac{1}{2}(\mathbf{x}-\mathbf{\mu})^T\mathbf{\Sigma}^{-1}(\mathbf{x}-\mathbf{\mu})}\]

	In particular, it is useful to be aware of the {\bf quadratic form} present in the exponential term of this equation:

	\[-\frac{1}{2}(\mathbf{x}-\mathbf{\mu})^T\mathbf{\Sigma}^{-1}(\mathbf{x}-\mathbf{\mu})\]

	\begin{framed}
	{\em \bf Correlation Coefficient}: Currently, our values for covariance depend on the units of measurement used for each variable. For example, imagine we have some variable X in metres, and then imagine we wish to change this to represent X in millimetres instead, our covariance would increase by three orders of magnitude! 
	\\
	\\
	This means we cannot directly compare covariance (for example to say X is more related to Y than it is to Z), and so it would be useful to define a measure which was independent of units used, so we could perform such comparisons. This measure is known as {\bf Pearson's correlation coefficient} (often shorted to just correlation coefficient), and is defined to be the following:

	\[\rho(x_i, x_j) = \rho_{ij} = \rho_{XY} = \frac{\sigma_{ij}}{\sqrt{\sigma_{ii}\sigma_{jj}}} = \frac{Cov(X,Y)}{\sigma_i\sigma_j}\]

	In words, the correlation coefficient between two variables \(X\) (assigned row \(i\) in our hypothetical matrix) and \(Y\) (assigned column \(j\)), is simply the covariance of \(X\) and \(Y\) divided by the product of their standard deviations (the square root of their variances).
	\\
	\\
	Dividing the covariance by the product of the standard deviations constrains the value of \(rho\) to between -1 and 1, inclusive (\(-1 \leq \rho \leq 1\)). Much like covariance, the value is positive if the variables are {\em directly} correlated, and negative if they are {\em inversely} correlated. For a value of 0, there is no correlation between variables. At a value of 1, there is a direct linear correlation between the variables; that is, Y increases as X does, and all the points lie on a line. Similarly at a value of -1, there is an inverse linear correlation, with Y decreasing as X increases and all points lying on a line.
	\\
	\\
	It is important to note that this correlation coefficient only tells us about the existence of a linear correlation, there may be other types of correlation present that this does not account for.
	\end{framed}

}