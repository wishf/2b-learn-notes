\renewcommand{\blockName}{Classification and k-nearest neighbour}

\renewcommand{\blockSynopsis}{
	In this chapter, we cover supervised training methods, specifically classification. We then investigate k-nearest neighbours as a method of classification.
}

\renewcommand{\blockContent}{
	\section{Supervision}

	In the last section, we covered clustering as a method of unsupervised training and briefly mentioned that supervised training did exist, but did not explore it in depth. So what is supervised training? A system that requires supervised training is one in which there are two sets of input data: a {\bf training set} of feature vectors, labelled with the desired output; and a {\bf test set} of feature vectors, which remains unlabelled. Typically, supervised systems are trained ahead of time and are fed test data as and when output is required.

	One commonly used type of supervised system is a {\bf classifier}. A classifier attempts to take an input vector and decide which of a number of groups it should belong to. In this chapter, we will be examining a simplistic classification algorithm: \(k\)-nearest neighbours.

	\section{Introducing KNN}

	\(k\)-nearest neighbours is a relatively simple and easy to understand algorithm. We load the entire training set into memory, and given a test point, attempt to find the \(k\) closest points within the training set. We then take the class which the largest amount of neighbours belong to and declare our test point to be in that class.

	The algorithm is very efficient in terms of training time, as a simple list append (which can be amortised to \(O(1)\)) to merge the new, classified points with the existing training set is all that is required.

	Defining \(X\) to be our training set, \(\mathbf{z}\) to be our test point, with a set \(C\) of possible classes and \(r\) as our distance metric (typically Euclidean), the K-nearest neighbours algorithm can be expressed precisely as follows:
		\begin{itemize}
			\item Compute \(r(\mathbf{z}, \mathbf{x}\) for every element \((\mathbf{x}, c)\) in our training set \(X\)
			\item Select the \(K\) elements from the training set which have the shortest distance to \(\mathbf{z}\), which we computed in the previous step. We can refer to this subset as \(X(k, \mathbf{z})\).
			\item The class of \(\mathbf{z}\) is simply the one which is most represented in the subset we created in the previous step, that is: \(c(\mathbf{z}) = {argmax}_{v \in \{1, \dots, C\}} \sum_{(\mathbf{x}, c) \in X(k, \mathbf{z})}\delta_{v,c}\)
		\end{itemize}

	In the above description, the \(\delta_{v,c}\) term is called the {\em Kronecker delta} and is a kind of {\em indicator variable} which is 1 when \(v = c\) and 0 otehrwise.

	Typically, we'll have to perform a distance test against every item in the training set, but there are data structures (such as the kd-tree) which allow us to reduce the amount of distance tests we perform. However, it tends to be the case that these data structures only offer significant speed improvements with fairly low-dimensional vectors, or when approximation are made. Research is on-going to see if performance can be improved further.

	The algorithm generates it's {\bf decision boundaries} based on local information. Decision boundaries are simply the boundaries between classes, points that lie on one side of the boundary belong to one class and points that lie on the other side belong to another. Rather than try to define boundaries globally (we will explore approaches later which do this), boundaries are established based on the information provided by only a few local points. This makes the algorithm very susceptible to noise and potentially over-dependant on local data structure to classify items for small K, and while these problems are reduced for larger values (seen by the smoothing of decision boundaries), they are not entirely eliminated.

	Decision boundaries produced by the algorithm are {\bf piecewise linear}. This is because each point effectively defines a small region around it where points are closer to it than any others (in the \(K=1\) case these are the classification boundaries for any test points). Our boundaries are defined then by the lines on the edge of these regions between points of different classes (hence linear). As we are dealing with many points, we connect these individual lines, and they become pieces of the overall decision boundary.
}