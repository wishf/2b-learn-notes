\renewcommand{\blockName}{Collaborative Filtering and Clustering}

\renewcommand{\blockSynopsis}{
	This chapter provides an overview on the kinds of machine learning we will talk about, and then explores clustering as a method of unsupervised machine learning, with \(K\)-means clustering as an example algorithm.
}

\renewcommand{\blockContent}{
	\section{How to Train Your Dragon}

	When talking about machine learning, it is important to differentiate between the way systems are trained. For some applications, we may wish to have specific types of output, having trained them against a set of data for which the output is known. For others, we may be speculatively trying to find patterns in a data set, with no data to train against.
	These are known as {\bf supervised} and {\bf unsupervised} training respectively.

	\section{Clustering}

	One category of unsupervised system is {\bf clustering}. Clustering systems work by grouping the data into subsets called {\em clusters}, based only on the input data without any training assitance. 

	Clustering is useful for two purposes:
		\begin{itemize}
			\item {\bf \em Data Interpretation} - Shedding light on the internal structure of the data
			\item {\bf \em Data Compression} - Finding efficient ways to code the data and represent it in a more compact format
		\end{itemize}

	Automated clustering systems have been used to cluster documents (for example, web pages) and many forms of scientific observational data (in fields such as astronomy and biology).

	\begin{framed}
	{\bf Clustering as Compression}: By using an automated clustering system, we can generalise data into groups. This can be useful for representing data. If we treat some point in each of our \(k\) clusters (typically the centre of the cluster) as a {\em prototype}, we can represent an arbitrary data point by some index from 1 to \(k\), referring to the cluster it belongs to, rather than providing a full \(n\)-dimensional vector. 

	This can greatly reduce the storage required to represent a large data set, but is a lossy form of compression, as each data point will not exactly match it's assigned prototype. 

	This is called {\bf vector quantisation} and in many cases this is a fair trade-off to make. For example, in image and video encoding, representing colours by storing a (relatively) small list of prototype colours and having each pixel index into this list makes storing this data a feasible prospect (uncompressed video at 1080p with reasonable bitrate can take up many gigabytes), and because each colour is close enough to what it should be, they are perceieved to be accurate enough by a human viewer (though being overzealous can lead to extensive artifacting, which is very obvious to human viewers and is to be avoided).
	\end{framed} 

	The category of clustering systems can be subdivided into two main sub-categories:
		\begin{itemize}
			\item {\bf \em Hierarchical} - Data is modelled as a tree of nested clusters, with a cluster at any point in the tree being the union of it's children
			\item {\bf \em Partitional} - The data has no nested structure, instead it is simply divided into a fixed number of non-overlapping clusters. Each data point in the set is used exactly once.
		\end{itemize}

	\begin{framed}
	{\bf Sub-categories of Hierarchical Clustering}: Hierarchical clustering methods can be further subdivided into two more main sub-categories:
		\begin{itemize}
			\item {\bf \em Top-down} - All points are in a top-level cluster that is recursively split into two or more sub-clusters until there is only one point in each cluster
			\item {\bf \em Agglomerative} - Effectively bottom-up; All points are in their own clusters, and the closest two clusters get merged each time until there is a single cluster remaining
		\end{itemize}
	\end{framed}

	Regardless of the type of clustering algorithm used, distance computations are the most common operation in clustering. This is because we want to find minimal distances from point to point in order to define the cluster a point belongs to.

	\section{K-Means Clustering}

	We will now look at the most commonly used partitional clustering algorithm: {\bf \(K\)-means clustering}. This algorithm aims to take a set of \(n\)-dimensional feature vectors and group them into \(K\) clusters, whether or not \(K\) clusters actually exist in the data.

	Each cluster is defined by choosing a centre point for it, and then each remaining point in the set of points is assigned to the cluster which has the closest center point. The center point of each cluster is then recomputed as the mean of the points in the cluster (the {\em centroid}) and the process is repeated. The process terminates once the location of the center points stabilises.

	Choosing the centre points can be done in multiple different ways, some examples of which are:
		\begin{itemize}
			\item Choose {\em random data points} as cluster centres
			\item Randomly assign data points to our \(K\) clusters and compute means as initial centres
			\item Choose data points from the set whose values are extreme
		\end{itemize}

	It can be seen that the algorithm requires some measure of distance to be defined in the vector space your data is expressed in. For this purpose, Euclidean distance is often use, but other measures of distance such as Manhattan distance are possible.

	Under \(K\)-means, convergence to a result is guaranteed, however the number of iterations (and consequently the time) it takes to achieve that is variable and not guaranteed. Because of this, it can be good to specify a maximum number of iterations for big datasets, especially as a good approximate solution can be achieved in many cases after only a few iterations.

	\section{Measuring Error}

	Now that we have our clusterings, it would be good if we had a way to compare them so we could work out which is better. To do so, we'd like to find the {\em error} in a clustering and minimise it. One way of doing this is to measure how spread out or scattered out the data in each cluster is. This measure is known as the {\bf \em mean squared error}.

	If we define a variable \(z_{nk}\) which is 1 if the \(n\)th data point \(\mathbf{x}^{(n)}\) belongs to cluster \(k\) (with mean \(\mathbf{m}^{(k)}\)) and 0 otherwise, then we can express the mean squared error as the following formula:

	\[E = \frac{1}{N}\sum_{k=1}^{K}\sum_{n=1}^{N}z_{nk}||\mathbf{x}^{(n)} - \mathbf{m}^{(k)}||^2\]

	In this formula, \(N\) is the number of points and \(K\) is the number of clusters.

	Another way of viewing this is is we are finding the squared deviation of data points belonging to each cluster, summed over every cluster. This can be seen as a variance measure, which leads to \(K\)-means sometimes being referred to as a {\em minimum variance clustering}.

	However, it is worthwhile to note that while the mean squared error only tells us how close points in each cluster are to each other, it does not tell us how far away points in a cluster are from the other clusters; there is no between-clusters term in the formula. Since the aim of clustering is to find some structure in a data set such that points in the same cluster are close, and far from points in other clusters, this measure of error does not give us an absolute method of comparing clusters.

	Different initialisations of the cluster centres cause the algorithm to converge to different solutions, each of which is a {\em local minima} of the error function, but are not necessarily the {\em global minimum}. The {\em online} (sample by sample) version, where a point is assigned and the centre updated immediately is less vulnerable to arriving at a local minima than the {\em batch} or {\em offline} version we have been covering. There are many more variants of \(K\)-means clusering designed to improve performance characteristics including runtime and error.

}