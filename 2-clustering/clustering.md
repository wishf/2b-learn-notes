- WHAT IS CLUSTER ANALYSIS
-- Unsupervised learning process (distinct from classification)
-- Aim is to group data into clusters based on only the data, there is no training data
-- Not necessarily any right answer

- REASONS FOR CLUSTERING
-- Shed light on how the data is structured; to aid understanding (data interpretation)
-- Find an efficient means of coding the data set; to represent it in a smaller form (data compression)

- DATA COMPRESSION
-- Represent each data item in a cluster by a single prototype
-- D-dimensional data in K clusters
-- Rather than represent each data point by a vector, represent it as it's cluster index
-- Called Vector quantisation
-- Trades some information for better space efficiency
-- Used in video, audio and image encoding

- CLUSTERING APPROACHES
-- Hierarchical - Tree of nested clusters; a cluster is the union of it's children
-- Partitional - Simply divide data into non-ovrlapping clusters, each data point in exactly 1.

- APPROACHES TO HEIRARCHICAL CLUSTERING
-- Top-down - Data points into one overarching cluster; split into two or more sub-clusters; repeat recursively until leaves are individual data points.
-- Agglomerative - Acts bottom-up. Each point acts as a single cluster and then merges closest clusters until a single cluster is obtained

- K MEANS CLUSTERING
-- Divide D-dimensional data points into K clusters, with K specified ahead of time.